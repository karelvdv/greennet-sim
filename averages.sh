#!/bin/bash
FILES=(`ls simulations/${1}/*_stats.csv`)
mkdir -p plots
python gnsim average ${FILES[@]} > "plots/${1}.csv"
