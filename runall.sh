#!/bin/bash
# Run all simulations that were done for the paper.

mkdir -p simulations/esnet/1tb_0.1s_short
mkdir -p simulations/esnet/1tb_1s_short
mkdir -p simulations/esnet/1tb_10s_short

mkdir -p simulations/esnet/1tb_0.1s_long
mkdir -p simulations/esnet/1tb_1s_long
mkdir -p simulations/esnet/1tb_10s_long

mkdir -p simulations/esnet/1tb_0.1s_uniform
mkdir -p simulations/esnet/1tb_1s_uniform
mkdir -p simulations/esnet/1tb_10s_uniform

python gnsim simulate --config=./conf_short.json --topology=./esnet_topology.json --energy=./esnet_energy.json --maxgb=1000 --mean=0.1 --out=./simulations/esnet/1tb_0.1s_short/

python gnsim simulate --config=./conf_short.json --topology=./esnet_topology.json --energy=./esnet_energy.json --maxgb=1000 --mean=1.0 --out=./simulations/esnet/1tb_1s_short/

python gnsim simulate --config=./conf_short.json --topology=./esnet_topology.json --energy=./esnet_energy.json --maxgb=1000 --mean=10 --out=./simulations/esnet/1tb_10s_short/


python gnsim simulate --config=./conf_long.json --topology=./esnet_topology.json --energy=./esnet_energy.json --maxgb=1000 --mean=0.1 --out=./simulations/esnet/1tb_0.1s_long/

python gnsim simulate --config=./conf_long.json --topology=./esnet_topology.json --energy=./esnet_energy.json --maxgb=1000 --mean=1.0 --out=./simulations/esnet/1tb_1s_long/

python gnsim simulate --config=./conf_long.json --topology=./esnet_topology.json --energy=./esnet_energy.json --maxgb=1000 --mean=10 --out=./simulations/esnet/1tb_10s_long/


python gnsim simulate --config=./conf_uniform.json --topology=./esnet_topology.json --energy=./esnet_energy.json --maxgb=1000 --mean=0.1 --out=./simulations/esnet/1tb_0.1s_uniform/

python gnsim simulate --config=./conf_uniform.json --topology=./esnet_topology.json --energy=./esnet_energy.json --maxgb=1000 --mean=1.0 --out=./simulations/esnet/1tb_1s_uniform/

python gnsim simulate --config=./conf_uniform.json --topology=./esnet_topology.json --energy=./esnet_energy.json --maxgb=1000 --mean=10 --out=./simulations/esnet/1tb_10s_uniform/
