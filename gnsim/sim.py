import argparse
import ujson as json
import os
import sys
import simpy as sp
import pandas as pd
import models
from collections import defaultdict
from datetime import datetime, timedelta
from random import Random
from scipy.special import gamma
from simpy.events import Event
from simpy.resources.container import Container
from time import time
from bunch import bunch


def betapdf(a, b, x):
	z = gamma(a + b) / gamma(a) / gamma(b)
	return x**(a - 1) * (1 - x)**(b - 1) / z

def betavars(a=None, b=None, m=0.5):
	if a is None:
		a = m / (1 - m) * b - (2 * m - 1) / (1 - m)
	elif b is None:
		b = a * (1 - m) / m - (1 - 2 * m) / m
	return a, b

def betadist(a=None, b=None, m=0.5):
	a, b = betavars(a, b, m)
	return lambda a=a, b=b: random.betavariate(a, b)

def carbon_mixes(carbon_emissions, mixes):
	def co2(sources):
		return sum(carbon_emissions[src] * pct for src, pct in sources.iteritems())
	return dict( (mix, co2(sources)) for mix, sources in mixes.iteritems() )

class ReserveEvent(Event):
	def __init__(self, sim, arrival, orderby, src, dst, gbytes, throughput):
		Event.__init__(self, sim.env)
		self.ok = True
		def postpone():
			t_next, ev_next = sim.filter_queue(ReleaseEvent)[0]
			ev = ReserveEvent(sim, arrival,
				orderby, src, dst, gbytes, throughput)
			sim.env.schedule(ev,
				priority=arrival,
				delay=.1 + t_next - sim.env.now)
		def drop():
			sim.modstat('ndropped', 1)
		def handle(self):
			paths = sim.network.paths(orderby, src, dst, gbytes, throughput)
			# filter out all paths that have < requested throughput
			if sim.param('full_throughput_always'):
				paths = [p for p in paths if not (throughput - p[1].throughput > 0.0)]
			# no path available
			if not paths:
				if sim.param('postpone'): postpone()
				else: drop()
				return
			path, metrics = paths[0]
			#dt = throughput - metrics.throughput
			## full throughput not available
			#if sim.param('full_throughput_always') and dt > 0.0:
			#	if sim.param('postpone'): postpone()
			#	else: drop()
			# reserve path and schedule release in the future
			#else:
			sim.network.reserve_path(path, metrics.throughput)
			sim.record_metrics()
			sim.modstat('nreserved', 1)
			if sim.env.now > arrival:
				sim.modstat('npostponed', 1)
			sink = sim.sink_add(metrics.throughput)
			sim.env.schedule(
				ReleaseEvent(sim, path, metrics.throughput, gbytes, sink),
				delay=metrics.hours * 3600.0,
				priority=0)
		self.callbacks = [handle]

class ReleaseEvent(Event):
	def __init__(self, sim, path, throughput, gbytes, sink):
		Event.__init__(self, sim.env)
		self.ok = True
		def handle(self):
			sim.network.release_path(path, throughput)
			sim.record_metrics()
			sim.sink_del(sink)
#			sim.modstat('total_throughput', throughput)
			sim.modstat('nreleased', 1)
		self.callbacks = [handle]

class Sink(object):
	def __init__(self, throughput, t0):
		self.rate = throughput
		self.t0 = t0
	def calc(self, t1):
		dt = t1 - self.t0
		self.t0 = t1
		return self.rate * dt

class Simulator(object):
	def __init__(self, env, network, startdate=None, params=None):
		self.env = env
		self.network = network
		self.t_last = env.now - 1
		self._stats = defaultdict(lambda: 0)
		self._timeseries_t = []
		self._timeseries_kw = []
		self._timeseries_cost = []
		self._timeseries_emissions = []
		#self._timeseries_bits = []
		self._params = params or {}
		self._startdate = startdate or datetime(2000, 1, 1)
		self._sinks = []
	def sink_add(self, throughput):
		sink = Sink(throughput, self.env.now)
		self._sinks.append(sink)
		return sink
	def sink_del(self, sink):
		self._sinks.remove(sink)
	def sink_total(self, now):
		return sum(sink.calc(now) for sink in self._sinks) / 8.0
	def filter_queue(self, evtype):
		return sorted((t, ev) for t,p,eid,ev in self.env._queue if isinstance(ev, evtype))
	def param(self, p, defv=None):
		return self._params.get(p, defv)
	def stat(self, stat, default=0):
		return self._stats[stat]
	def modstat(self, stat, v):
		self._stats[stat] += v
	def record_metrics(self):
		t_now = self.env.now
		if not (t_now > self.t_last):
			return
		realtime = self._startdate + timedelta(seconds=t_now)
		kw, cost, emissions, bits = self.network.total_metrics()
		self._timeseries_t.append(realtime)
		self._timeseries_kw.append(kw)
		self._timeseries_cost.append(cost)
		self._timeseries_emissions.append(emissions)
		#self._timeseries_bits.append(bits)
		hours = (t_now - self.t_last) / 3600.0
		self.modstat('total_kwh', hours * kw)
		self.modstat('total_emissions', hours * emissions)
		self.modstat('total_cost', hours * cost)
		#self.modstat('total_bits', hours * bits)
		self.t_last = t_now
		traffic = self.sink_total(t_now)
		self.modstat('total_gbytes', traffic)
		if self.stat('total_gbytes') >= self.param('max_gbytes'):
			sp.core._stop_simulate(None)
	def timeseries(self):
		index = pd.DatetimeIndex(data=self._timeseries_t)
		return pd.DataFrame(
			dtype='float32',
			index=index,
			data=dict(
				energy=self._timeseries_kw,
				cost=self._timeseries_cost,
				emissions=self._timeseries_emissions,
				#bits=self._timeseries_bits,
			)
		)
	def _recorder(self, interval):
		ival = interval
		nreleased = self.stat('nreleased')
		while True:
			yield self.env.timeout(ival)
			self.record_metrics()
			nr = self.stat('nreleased')
			if nr > nreleased:
				ival = interval
			else:
				ival += interval
			nreleased = nr
	def _producer(self, circuits):
		for p in circuits:
			yield self.env.timeout(p.arrival)
			arrival_time = self.env.now
			ev = ReserveEvent(self, arrival_time,
				p.orderby, p.src, p.dst, p.gbytes, p.throughput)
			self.env.schedule(ev, priority=arrival_time )
	def run(self, circuits, until=None, record_interval=10.0):
		self.record_metrics()
		self.env.process( self._producer(circuits) )
		self.env.process( self._recorder(record_interval) )
		self.env.run(until)
		self.record_metrics()
		self.modstat('nwaiting', len(self.filter_queue(ReserveEvent)))
		self.modstat('nunfinished', len(self.filter_queue(ReleaseEvent)))
		self.modstat('simtime', self.env.now)
#		self.modstat('avg_throughput',
#			self.stat('total_throughput') / self.stat('nreleased') )
		return self


def exponential_distribution(lambd, seed=None):
	rng = Random(seed)
	return lambda rng=rng, lambd=lambd: rng.expovariate(lambd)

def beta_distribution(a=None, b=None, c=0.0, s=1.0, seed=None):
	rng  = Random(seed)
	return lambda rng=rng, a=a, b=b, c=c, s=s: c + s * rng.betavariate(a, b)

def sampler(lst, n, seed=None):
	rng = Random(seed)
	return lambda n=n, lst=lst, rng=rng: rng.sample(lst, n)

def go(topology, energy, co2_mixes, kwh_rates, config, runparams, seed):#rng):
	env = sp.Environment()

	topology = models.Topology(
		nodes=topology['nodes'],
		edges=topology['edges'],
		resource=lambda cap: Container(env, cap, cap),
	)

	energy_model = models.StaticEnergyModel(
		co2_mixes=co2_mixes,
		kwh_rates=kwh_rates,
	)

	utilization_model = models.LinearUtilizationModel

	power_model = models.LinearPowerModel(
		utilization_model=utilization_model,
		d=runparams['d'],
	)

	loss = float(config['ethernet'].get('loss', 0.0))
	if loss > 0.0:
		mss = int(config['ethernet'].get('mss', 8840))
		c   = int(config['ethernet'].get('c', 1.0))
		ethernet_model = models.LossyEthernetModel(loss, mss, c)
	else:
		ethernet_model = models.LosslessEthernetModel

	network = models.NetworkModel(
		topology=topology,
		energy_model=energy_model,
		power_model=power_model,
		ethernet_model=ethernet_model,
	)

	random_pair     = sampler(topology.endpoints(), 2, seed)#rng.random())
	gbytes_dist     = read_betadist(config['data'], seed)#rng.random())
	throughput_dist = read_betadist(config['throughput'], seed)#rng.random())
	lambd           = 1. / float(config['arrival_mean'])
	arrival_dist    = exponential_distribution(lambd, seed)#rng.random())

	def gencircuits(orderby, max=0):
		count = 0
		while not max or (max and count < max):
			count += 1
			src, dst = random_pair()
			yield bunch(
				arrival=arrival_dist(),
				orderby=orderby,
				src=src,
				dst=dst,
				gbytes=round(gbytes_dist(), 1),
				throughput=round(throughput_dist(), 1),
			)

	sim = Simulator(env, network,
			params=dict(
				full_throughput_always=bool(config.get('full_throughput_always', False)),
				postpone=0,
				max_gbytes=config['max_gbytes'],
			)
		)
	orderby = runparams['metric']
	count = int(config.get('count', 0))
	until = int(config.get('until', 0))
	until = until if until > 0 else None
	circuits = gencircuits(orderby, count)
	return sim.run(circuits,
		until=until,
	)

def read_betadist(config, seed):
	a = float(config.get('alpha', 0.0))
	b = float(config.get('beta', 0.0))
	m = float(config.get('mean', 0.5))
	c = float(config.get('base', 0.0))
	s = float(config.get('scale', 1.0))
	a, b = betavars(a, b, m)
	return beta_distribution(a=a, b=b, c=c, s=s, seed=seed)

from functools import partial
def round_dataframe(df, decimals):
	f = partial(pd.Series.round, decimals=decimals)
	return df.apply(f)

def parseargs(args):
	p = argparse.ArgumentParser()
	psub = p.add_subparsers(dest='command', help='sub-commands')
	psim = psub.add_parser('simulate', help='simulation')
	psim.add_argument('--config', help='Simulation configuration file.', default='./config.json')
	psim.add_argument('--topology', help='Topology file.', default='./topology.json')
	psim.add_argument('--energy', help='Energy production file.', default='./energy.json')
	psim.add_argument('--out', help='output dir.', default='.')
	psim.add_argument('--until', help='simulation time.', default=None)
	psim.add_argument('--count', help='total requests to handle.', default=0)
	psim.add_argument('--maxgb', help='max gbytes to transport.', default=0)
	psim.add_argument('--mean', help='arrival mean.', default=0)
	pplt = psub.add_parser('plot', help='plot timeseries')
	pplt.add_argument('config', help='plot config')
	pavg = psub.add_parser('average', help='average stats')
	pavg.add_argument('--dir', help='dir', default='.')
	pavg.add_argument('files', help='_stats files', nargs='+')
	return p.parse_args(args)

def main(args):
	args = parseargs(args)
	# do simulations
	if args.command == 'simulate':
		config    = json.loads(open(args.config).read())
		topo      = json.loads(open(args.topology).read())
		energy    = json.loads(open(args.energy).read())
		co2_mixes = carbon_mixes(energy['carbon_emissions'], energy['energy_mixes'])
		kwh_rates = energy['kwh_rates']

		if args.until:
			config['until'] = int(args.until)
		if args.count:
			config['count'] = int(args.count)
		if args.maxgb:
			config['max_gbytes'] = int(args.maxgb)
		if args.mean:
			config['arrival_mean'] = float(args.mean)
		out = args.out

		runs = config['runs']

		for runparams in runs:
			repeats = int(runparams.get('repeat', 1))
			prefix  = os.path.join(out, runparams['prefix'])
			seed    = runparams['seed']
			rng     = Random(seed)
			seeds   = [rng.randint(0, 1000000) for _ in xrange(repeats)]
			statistics = []
			for repeat in xrange(repeats):
				#rng = Random(seeds[repeat])
				sim = go(topo, energy, co2_mixes, kwh_rates, config, runparams, seeds[repeat])#rng)
				stats = dict(sim._stats)
				stats['seed'] = seeds[repeat]
				statistics.append(stats)

				resample = runparams.get('resample', None)
				ts = sim.timeseries()
				if resample:
					ts = ts.resample(resample, how='mean')
					ts = ts.dropna()
				fname = prefix + ('_ts%d.csv' % repeat)
				ts.to_csv(fname)
				print fname
			dfstat = pd.DataFrame(statistics)
			dfstat.to_csv(prefix + '_stats.csv')
	# plot time series
	elif args.command == 'plot':
		import pylab as plt
		config = json.loads(open(args.config).read())
		resample = config.get('resample', None)
		for plot in config['plots']:
			df = pd.io.parsers.read_csv(plot['file'], index_col=0, parse_dates=True)
			if resample:
				df = df.resample(resample, 'mean')
				df = df.dropna()
			plt.plot(df.index, df[ plot['column'] ], color=plot['color'])
		plt.show()
	# average the stats
	elif args.command == 'average':
		fs  = [os.path.join(args.dir, f) for f in args.files]
		csv = [pd.io.parsers.read_csv(f, index_col=0) for f in fs]
		z   = zip(fs, csv)
		mean = dict( (os.path.split(f)[-1]+'_mean', df.mean()) for f,df in z)
		std  = dict( (os.path.split(f)[-1]+'_std', df.std()) for f,df in z)
		df = pd.concat( [pd.DataFrame(mean), pd.DataFrame(std)], axis=1).T
		df = round_dataframe(df, 2)
		df.to_csv(sys.stdout)

if __name__ == '__main__':
	main(sys.argv[1:])
