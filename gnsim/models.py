import networkx as nx
from math import sqrt
from itertools import izip, combinations
from collections import defaultdict
from bunch import bunch

class Topology(object):
	"""Describes the topology and gives access to relevant node/edge properties."""
	def __init__(self, nodes, edges, resource):
		def mk_node(name, attrs):
			cap = attrs['capacity']
			return (name, dict(
				bndw=resource(cap),
				sink=0,
				attrs=bunch(**attrs)
			))
		self.graph = nx.Graph()
		self.graph.add_nodes_from( mk_node(k, v) for k,v in nodes.iteritems() )
		self.graph.add_edges_from( edges )
		nodes = self.graph.nodes()
		self.__endpoints = [n for n in nodes if self.graph.node[n]['attrs']['endpoint']]
		if not self.__endpoints:
			self.__endpoints = nodes
		# memoize paths
		self.__simple_paths = defaultdict(lambda: defaultdict(lambda: None))
	def endpoints(self):
		return self.__endpoints
	def rtt(self, a, b):
		"""round trip time on a <-> b edge."""
		return self.graph[a][b]['rtt']
	def bandwidth(self, node):
		return self.graph.node[node]['bndw']
	def bits(self, node):
		"""bits currently flowing through the node"""
		bndw = self.graph.node[node]['bndw']
		return bndw.capacity - bndw.level
	def has_node(self, a):
		return self.graph.has_node(a)
	def has_edge(self, a, b):
		return self.graph.has_edge(a, b)
	def node(self, k):
		return self.graph.node[k]['attrs']
	def nodes(self):
		return self.graph.nodes()
	def edges(self):
		return self.graph.edges()
	def all_pairs(self):
		return combinations(self.nodes(), 2)
	def all_simple_paths(self, src, dst):
		paths = self.__simple_paths[src][dst]
		if not paths:
			paths = sorted(nx.all_simple_paths(self.graph, src, dst), key=len)
			self.__simple_paths[src][dst] = paths
		return paths
	def all_all_simple_paths(self):
		paths = {}
		for a, b in self.all_pairs():
			simple_paths = self.all_simple_paths(a, b)
			paths[(a, b)] = simple_paths
			paths[(b, a)] = [p[::-1] for p in simple_paths]
		return paths
	def all_shortest_paths(self):
		return (nx.shortest_path(self.graph, a, b) for a,b in self.all_pairs())

class StaticEnergyModel(object):
	"""Energy model where energy production mixes and kWh rates do not change."""
	def __init__(self, co2_mixes, kwh_rates):
		self.co2_mixes = co2_mixes
		self.kwh_rates = kwh_rates
	def emissions(self, region):
		return self.co2_mixes[region]
	def kwh(self, region):
		return self.kwh_rates[region]

class NetworkModel(object):
	"""Combines topology, power model, and energy model to enable path finding."""
	def __init__(self, topology, power_model, energy_model, ethernet_model):
		self.topology = topology
		self.power_model = power_model
		self.energy_model = energy_model
		self.ethernet_model = ethernet_model
	def load(self, node, offset=0.0):
		bndw = self.topology.bandwidth(node)
		return 1.0 - (float(bndw.level - offset) / float(bndw.capacity))
	def bandwidth_free(self, node):
		return self.topology.bandwidth(node).level
	def power(self, node, load, pstatic_mult):
		return self.power_model(self.topology, node, load, pstatic_mult)
	def emissions(self, node, pwr):
		return pwr * self.energy_model.emissions(self.topology.node(node).region)
	def cost(self, node, pwr):
		return pwr * self.energy_model.kwh(self.topology.node(node).region)
	def total_metrics(self):
		emissions = 0.0
		cost      = 0.0
		kw        = 0.0
		bits      = 0.0
		for node in self.topology.nodes():
			pwr        = self.power(node, self.load(node), 1.0)
			emissions += self.emissions(node, pwr)
			cost      += self.cost(node, pwr)
			bits      += self.topology.bits(node)
			kw        += pwr
		return kw, cost, emissions, bits
	def metrics(self, path, gbytes, throughput, pstatic_mult):
		loads = [self.load(node, throughput) for node in path]
		# invalid path: throughput exceeds path capacity
		if any(load > 1.0 for load in loads):
			# check if path is free at reduced throughput
			minbndw = min(self.bandwidth_free(node) for node in path)
			if minbndw > 0.0:
				return self.metrics( path, gbytes, minbndw, pstatic_mult )
			# no bandwidth available, give up
			return None
		hours     = (1. / 3600) / throughput
		powers    = [self.power(nd, loads[i], pstatic_mult) for i,nd in enumerate(path)]
		emissions = sum(self.emissions(nd, pwr) for nd,pwr in izip(path, powers))
		cost      = sum(self.cost(nd, pwr) for nd,pwr in izip(path, powers))
		gbits     = 8 * gbytes
		return bunch(
			throughput=throughput,
			hours=hours * gbits,
			energy=sum(powers) * hours * gbits,
			emissions=emissions * hours * gbits,
			cost=cost * hours * gbits,
			gbits=gbits,
			hops=len(path) - 1,
		)
	def paths(self, orderby, src, dst, gbytes, throughput, pstatic_mult=1.0):
		paths    = self.topology.all_simple_paths(src, dst)
		metrics  = [self.metrics(p, gbytes,
			self.ethernet_model(self.topology, p, throughput), pstatic_mult)
			for p in paths]
		stuff = [(a, b) for a,b in izip(paths, metrics) if b]
		stuff.sort(key=lambda v: v[1][orderby])
		return stuff
	def reserve_path(self, path, throughput):
		topo = self.topology
		for node in path:
			topo.bandwidth(node).get(throughput)
	def release_path(self, path, throughput):
		topo = self.topology
		for node in path:
			topo.bandwidth(node).put(throughput)

def mathis_equation(rtt, loss, mss, c):
	return ((mss * 8) / (rtt * .001)) * (c / sqrt(loss))

def LosslessEthernetModel(topo, path, throughput):
	return throughput

def LossyEthernetModel(loss, mss, c):
	def fn(topo, path, throughput):
		edges = izip(path, path[1::])
		rtt   = sum(topo.rtt(s, d) for s,d in edges)
		bndw  = min(topo.bandwidth(n).capacity for n in path)
		return min(throughput, bndw, mathis_equation(rtt, loss, mss, c) / 10**9)
	return fn

def LinearPowerModel(utilization_model, d):
	def fn(topo, node, load, pstatic_mult):
		n    = topo.node(node)
		psta = n.power_static
		pdyn = n.power_dynamic
		pwr  = pstatic_mult * (psta + pdyn * (1 - d)) \
		     + pdyn * d * utilization_model(load)
		return n.pue * pwr
	return fn

def LinearUtilizationModel(u):
	return u

# paper: energy-oriented models for wdm networks
def CombinedUtilizationModel(t1=0.33, t2=0.66, a=1.0, b=0.67, c=2.0):
	def fn(u):
		if u < t1:
			return u * a
		elif u < t2:
			base = t1 * a
			return base + (u ** b) - (t1 ** b)
		else:
			base = t1 * a + (t2 ** b) - (t1 ** b)
			return base + (c ** u) - (c ** t2)
	return fn
