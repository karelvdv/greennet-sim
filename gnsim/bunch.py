class bunch(object):
	def __init__(self, **kwargs):
		self.__dict__.update(**kwargs)
	def __delitem__(self, k):
		return self.__dict__.__delitem__(k)
	def __getitem__(self, k):
		return self.__dict__.__getitem__(k)
	def __setitem__(self, k, v):
		return self.__dict__.__setitem__(k, v)
	def __contains__(self, k):
		return self.__dict__.__contains__(k)
	def __iter__(self):
		return self.__dict__.iteritems()
	def __repr__(self):
		return self.__dict__.__repr__()
